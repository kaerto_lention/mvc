//
//  ChatCell.swift
//  MVC
//
//  Created by Viktor on 5/14/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation
import UIKit

class ChatCell: UITableViewCell
{
    @IBOutlet weak var senderName: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
