//
//  ChatViewController.swift
//  MVC
//
//  Created by Viktor on 5/14/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation
import UIKit

class ChatViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var chatTable: UITableView!
    @IBOutlet weak var messageBox: UITextField!
    @IBOutlet weak var sendButton: UIButton!
    
    var user: User?
    var messageCounter = 0
    var messages = [Message]()
    var conversation = Conversation()
    
    var timer:NSTimer! = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(ChatViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
        
        sendButton.addTarget(self, action: #selector(ChatViewController.sendMessage), forControlEvents: UIControlEvents.TouchUpInside)
        
        chatTable.dataSource = self
        chatTable.delegate = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.loadHistory(_:)), name: Notification.HistoryLoaded, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(ChatViewController.startChat(_:)), name: Notification.ChatisOpen, object: nil)
        
    }
    
    func startChat(notification:NSNotification) {
        SlackManager.sharedInstance.getMessages(notification.userInfo!["chatID"] as! String)
        conversation.id = notification.userInfo!["chatID"] as! String
    }
    
    func refreshHistory(){
        SlackManager.sharedInstance.getMessages(conversation.id)
    }
    
    
    func loadHistory(notification:NSNotification) {
        messages = notification.userInfo!["messages"] as! [Message]
        chatTable.reloadData()
        if (messages.count != messageCounter){
            self.chatTable.scrollToRowAtIndexPath(NSIndexPath(forRow: self.messages.count-1, inSection: 0), atScrollPosition: .Bottom, animated: true)
        }
        self.messageCounter = messages.count
        timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: #selector(ChatViewController.refreshHistory), userInfo:nil, repeats: false)
    }
    
    override func viewWillAppear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = true
        navigationItem.title = user!.name
    }
    
    func keyboardWillShow(notification: NSNotification) {
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.view.frame.origin.y -= keyboardSize.height
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.CGRectValue() {
            self.view.frame.origin.y += keyboardSize.height
        }
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
    }
    
    func sendMessage()
    {
        let msg = Message(); msg.text = messageBox.text! ; msg.isMine = true
        messages.append(msg)
        chatTable.reloadData()
        SlackManager.sharedInstance.postMessage(conversation.id, text:msg.text)
        
        let delay = 0.1
        let time = dispatch_time(DISPATCH_TIME_NOW, Int64(delay))
        
        dispatch_after(time, dispatch_get_main_queue(), {
            let indexPath = NSIndexPath(forRow: self.messageCounter - 1, inSection: 0)
            self.chatTable.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
            self.messageBox.text=""
            self.dismissKeyboard()
        })
        messageCounter += 1
    }
    
    func tableView(tblView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let msg = messages[indexPath.row]
        var cellIdentifier = "ChatCellR"
        if (msg.isMine) {
            cellIdentifier = "ChatCell"
        }
        let cell = tblView.dequeueReusableCellWithIdentifier(cellIdentifier) as! ChatCell
        
        if (msg.isMine){
            cell.messageLabel.text = msg.text
            cell.senderName.text = "Me:"
        }
        else {
            cell.messageLabel.text = msg.text
            cell.senderName.text = user!.name
        }
        return cell
    }
    
    func tableView(tblView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return messages.count
    }
}


















