//
//  Controller.swift
//  MVC
//
//  Created by MacBookPro2 on 5/12/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation




public class Controller: NSObject {
    var view : MainView!
    public var model:Model!
    
    public init(view:MainView) {
        super.init()
        self.view = view
        self.model = Model()
    }
}