//
//  Notification.swift
//  MVC
//
//  Created by MacBookPro2 on 5/12/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation

public struct Notification{
    static let LoggedIn = "user.loggedIn"
    static let UsersFounds = "user.friends.found"
    static let HistoryLoaded = "chat.messages.loaded"
    static let ChatisOpen = "chat.open"
}