//
//  SlackManager.swift
//  testSlack
//
//  Created by MacBookPro2 on 5/14/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
class SlackManager {
    static let sharedInstance = SlackManager()
    
    var code:String!
    var token:String!
    var isLoggedIn = false
    
    var currentUser = User()
    var currentChat = Conversation()
    var userList = [User]()
    
    
    let client_id = "42969857697.42967123152"
    let client_secret = "d578b9fcdfca9e73f184edf72f27c27d"
    let redirect_uri = "chatOver://"
    
    private init(){
    }
    
    func connect() -> Void {
        let request =
            Alamofire.request(.GET, "https://slack.com/oauth/authorize", parameters:
                ["client_id":client_id, "redirect_uri":redirect_uri, "scope":"client"] )
        print(request.request!.URL)
        UIApplication.sharedApplication().openURL(request.request!.URL!)
        request.cancel()
    }
    
    
    func setCode(token:NSURL) -> Void {
        let urlComponents = NSURLComponents(string: token.absoluteString)
        let queryItems = urlComponents?.queryItems
        let codet = queryItems?.filter({$0.name == "code"}).first
        code = codet?.value
        self.getToken(code)
    }
    
    func getToken(code:String) -> Void {
        Alamofire.request(.GET, "https://slack.com/api/oauth.access",
            parameters: ["client_id":client_id,"client_secret":client_secret, "code":code]).responseJSON { response in
                if let JSON = response.result.value {
                    self.token = JSON["access_token"] as! String
                    self.isLoggedIn = true
                    self.currentUser.id = JSON["user_id"] as! String
                    self.currentUser.team.id = JSON["team_id"] as! String
                    self.currentUser.team.name = JSON["team_name"] as! String
                    self.findUsers()
                }
        }
    }
    // MARK:
    // MARK: Find team users
    func findUsers() -> Void{
        if !isLoggedIn { return }
        Alamofire.request(.GET, "https://slack.com/api/users.list", parameters: ["token":token, "presence":"1"]).responseJSON {
            response in
            if let JSON = response.result.value {
                var i = (JSON["members"] as? [AnyObject])!.count
                while (i>0){
                    i-=1
                    var dict = JSON["members"] as? [AnyObject]
                    let temp = User()
                    temp.color = dict![i]["color"] as! String
                    temp.id = dict![i]["id"] as! String
                    temp.name = dict![i]["name"] as! String
                    self.userList.append(temp)
                }
                NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: Notification.UsersFounds,
                    object: self))
            }
        }
    }
    
    // MARK:
    // MARK: Chat methods
    func startChat(nr:Int) -> Void {
        Alamofire.request(.GET, "https://slack.com/api/im.open",
            parameters: ["token":token,"user":userList[nr].id]).responseJSON { response in
                if let JSON = response.result.value {
                    self.currentChat.id = JSON["channel"]!!["id"] as! String
                    NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: Notification.ChatisOpen,
                        object: self, userInfo: ["chatID":self.currentChat.id]))
                }
        }
    }
    
    func getMessages(chatID:String) -> Void {
        Alamofire.request(.GET, "https://slack.com/api/im.history",
            parameters: ["token":token,"channel":chatID, "count":"10"]).responseJSON { response in
                if let JSON = response.result.value {
                    var messageList = [Message]()
                    if var dict = JSON["messages"] as? [AnyObject]{
                        var i = dict.count
                        while (i>0){
                            i-=1
                            if (dict[i]["type"] as! String == "message"){
                                let temp = Message()
                                temp.text = dict[i]["text"] as! String
                                temp.owner.id = dict[i]["user"] as! String
                                temp.isMine = ((dict[i]["user"] as! String) == self.currentUser.id)
                                messageList.append(temp)
                            }
                        }
                    }
                    NSNotificationCenter.defaultCenter().postNotification(NSNotification(name: Notification.HistoryLoaded,
                        object: self, userInfo: ["messages":messageList]))
                    
                }
        }
    }
    
    func postMessage(chatID:String, text:String) -> Void {
        Alamofire.request(.POST, "https://slack.com/api/chat.postMessage",
                          parameters: ["token":token,"channel":chatID, "text":text, "as_user":"true"])
    }
    
}








