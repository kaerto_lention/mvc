//
//  UserViewCell.swift
//  MVC
//
//  Created by Viktor on 5/14/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation
import UIKit

class UserViewCell : UITableViewCell
{
    @IBOutlet weak var userNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}