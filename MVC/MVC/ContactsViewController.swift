//
//  ContactsViewController.swift
//  MVC
//
//  Created by Viktor on 5/14/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation
import UIKit

class ContactsViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    @IBOutlet weak var tableView: UITableView!
    
    var users = [User]()
    var contactIndex: Int?

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        loadUsers()
    }

    func loadUsers()
    {
        users = SlackManager.sharedInstance.userList
        tableView.reloadData()
        print("Load Users")
    }
    
    /*
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toChat"
        {
            var chatView = segue!.destinationViewController as ChatViewController
            chatView.userName =
        }
    }
    */
    
    override func viewWillAppear(animated: Bool) {
        self.tabBarController?.tabBar.hidden = false
        navigationItem.title = "Contacts"
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(ContactsViewController.loadUsers) , name: Notification.UsersFounds, object: nil)
    }
    
    func tableView(tblView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = tblView.dequeueReusableCellWithIdentifier("userCell") as! UserViewCell
        let user = users[indexPath.row]
        cell.userNameLabel.text = user.name
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        //NSLog("You selected cell number: \(indexPath.row)!")
        contactIndex = indexPath.row
        self.performSegueWithIdentifier("toChat", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "toChat"
        {
            let chatView = segue.destinationViewController as! ChatViewController
            chatView.user = users[contactIndex!]
            SlackManager.sharedInstance.startChat(contactIndex!)
            
        }
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func tableView(tblView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return users.count
    }
}




















