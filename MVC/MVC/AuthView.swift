//
//  AuthView.swift
//  MVC
//
//  Created by Viktor on 5/14/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation
import UIKit

class AuthView:UIViewController
{
    @IBOutlet weak var authButton: UIButton!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        authButton.addTarget(self, action: #selector(AuthView.authClick), forControlEvents: UIControlEvents.TouchUpInside)
        spinner.hidden = true;
    }
    
    func authClick()
    {
        spinner.hidden = false;
        spinner.startAnimating()
        SlackManager.sharedInstance.connect()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(AuthView.goForward), name: Notification.LoggedIn, object:nil)
        
    }
    
    
    func goForward(){
        NSNotificationCenter.defaultCenter().removeObserver(self)
        self.performSegueWithIdentifier("authSegue", sender: self)
    }
}