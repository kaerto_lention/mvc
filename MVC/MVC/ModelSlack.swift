//
//  ModelSlack.swift
//  testSlack
//
//  Created by MacBookPro2 on 5/14/16.
//  Copyright © 2016 wasea. All rights reserved.
//

import Foundation

class User {
    var name = ""
    var id = ""
    
    var color = ""

    var team = Team()
}

class  Team {
    var id = ""
    var name = ""
}

class Message  {
    var owner = User()
    var text = ""
    var isMine = true
}

class Conversation {
    var id = ""
    var user = User()
}